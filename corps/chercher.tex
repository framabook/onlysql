\newcommand{\valrech}[1][V]{\ensuremath{\mathcal{#1}}}
\section{Chercher}
\label{sec-sda-chercher}

Dans cette partie nous nous intéresserons aux algorithmes fondamentaux
de \emph{recherche}. La recherche est une tâche réalisée extrêmement
fréquemment par la plupart des logiciels. Par exemple, lorsque que
votre logiciel de traitement de texte souligne en rouge un mot pour
indiquer qu'il est mal orthographié, il vient d'effectuer une
recherche parmi les quelques dizaines de milliers de mots de son
dictionnaire.

Le problème de base que résolvent les algorithmes de recherche peut
s'énoncer simplement : étant donné un ensemble de données, est-ce que
la valeur~\valrech{} est présente dans cet ensemble ? En examinant
différentes méthodes de recherche nous verrons que la structure de
données sous-jacente, c'est-à-dire l'\emph{organisation} des données,
permet d'effectuer des recherches de façon plus ou moins
efficace. Comme lorsque vous cherchez un livre dans votre
bibliothèque, vous mettrez énormément de temps à trouver un exemplaire
si vos livres sont rangés sans aucune logique et vous mettrez
rapidement la main sur un cd\footnote{Souvenez-vous, ces petites
  galettes en plastique avec un trou au milieu.} de Frank
\textsc{Zappa} si vous avez classé vos disques par ordre alphabétique
des auteurs.

\subsection{Chercher dans une séquence}
\label{sec-sda-recherche-sequence}
\index{recherche!séquentielle}

\newcommand{\seqscale}{0.5}

\begin{nota}
  On suppose ici que la structure de donnée «~séquence~» est une
  collection homogène de données à laquelle on accède via un numéro de
  case. Cet accès est supposé se faire en temps constant, c'est-à-dire
  via le même temps quelle que soit la case accédée.
\end{nota}

\subsubsection{Dans une séquence non triée}

Dans un premier temps on fait l'hypothèse que les données sont
organisées dans une \emph{séquence non triée}. Par exemple :
\begin{center}
  \includegraphics[scale=\seqscale]{sda-sequence-exemple}
\end{center}

Si la séquence est non triée, il n'y a pas d'autre alternative que de
parcourir chaque élément à partir du début de la séquence et de le
comparer avec la valeur qu'on recherche. On arrêtera le parcours
dès qu'on la trouve ou qu'on arrive en fin de séquence.
\iffalse\begin{algo}[noendkeyword]
  \BEGIN
  \STATE{trouvé\recoit{}FAUX}
  \STATE{i\recoit{}0}
  \WHILE{Pas trouvé ET Pas i<NbElts(S)}
  \IF{S[i]=\valrech{}}
  \STATE{trouvé\recoit{}vrai}
  \ENDIF
  \STATE{i\recoit{}i+1}
  \ENDWHILE
  \END
\end{algo}\fi
La figure ci-dessous montre deux recherches, l'une ---~fructueuse~--- de
la valeur~11, l'autre ---~infructueuse~--- de la valeur~4 :
\begin{center}
  \includegraphics[scale=\seqscale]{sda-recherche-non-triee}
\end{center}
Pour analyser la complexité d'un tel algorithme, on choisit d'estimer
le nombre de comparaisons. On peut noter que dans le pire des cas, il
faudra parcourir toute la séquence (cas d'une recherche infructueuse).
En outre, pour chercher une valeur~\valrech{} prise au hasard on peut
dire dans une séquence composée de valeurs aléatoires, on fera en
moyenne de $(N+1)/2$ comparaisons.  On peut donc dire que :
\begin{propriete}
  La complexité de la recherche dans une séquence non triée est $O(N)$
  en moyenne et dans le pire des cas. 
\end{propriete}

\subsubsection{Dans une séquence triée}

Dans une séquence triée comme celle-ci :
\begin{center}
  \includegraphics[scale=\seqscale]{sda-sequence-triee-exemple}
\end{center}
on pourra chercher une valeur \valrech{} en effectuant un parcours de
la séquence et en s'arrêtant dès que l'on rencontre la valeur~\valrech{},
ou la fin de la séquence, ou une valeur supérieure à~\valrech{}. 
\iffalse D'où
l'algorithme :
\begin{algo}[noendkeyword]
  \BEGIN
  \STATE{eltcourant\recoit{}1\ier{}\_élément($S$)}
  \STATE{trouvé\recoit{}(valeur(eltcourant)=\valrech{})}
  \WHILE{Pas trouvé ET Pas fin($S$) ET \fbox{\valrech{}>=valeur(eltcourant)}}
  \IF{valeur(eltcourant)=\valrech{}}
  \STATE{trouvé\recoit{}vrai}
  \ENDIF
  \STATE{eltcourant\recoit{}suivant(eltcourant)}
  \ENDWHILE
  \RETURN{trouvé}
  \END
\end{algo}\fi
On pourrait penser que le fait que la séquence soit triée diminue le
temps de la recherche, mais il suffit que la valeur~\valrech{} soit
située en fin de séquence pour qu'on ait à parcourir la quasi-totalité
des données :
\begin{center}
  \includegraphics[scale=\seqscale]{sda-recherche-triee}
\end{center}
On s'intéressera ici aussi à estimer le nombre de comparaisons. On
peut montrer que :
\begin{propriete}
  La complexité de la recherche dans une séquence triée est en $O(N)$ en
  moyenne et dans le pire des cas. 
\end{propriete}

\subsubsection{Recherche dichotomique}
\index{recherche!dichotomique}
\index{dichotomique (recherche)}

La recherche dichotomique est la méthode utilisée dans le jeu où un
joueur fait deviner un nombre secret (entre 1 et~100 par exemple) à un
autre joueur. Ce dernier propose le nombre qu'il veut et le premier
joueur lui répond soit qu'il s'agit d'un nombre trop grand, soit trop
petit, soit du nombre secret. Dans le cas d'un nombre secret entre 1
et~100, il est évident que la manière de deviner le \emph{plus
  rapidement possible} le nombre consiste à choisir d'abord le
nombre~50, puis 75 ou 25 selon le cas, etc.  

\iffalse On peut donc énoncer
l'algorithme de la recherche dichotomique pour une valeur \valrech{}
dans une séquence triée~$S$ de façon récursive :
\begin{algo}
  \BEGIN
  \STATE{$\mathcal{M}$\recoit{}la valeur du \emph{milieu} de $S$}
  \IF{$\valrech{}=\mathcal{M}$}
  \RETURN{vrai}
  \ELSEIF{$\valrech{}>\mathcal{M}$}
  \STATE{recherche \emph{récursive} dans la \textbf{moitié droite}}
  \ELSE
  \STATE{recherche \emph{récursive} dans la \textbf{moitié gauche}}
  \ENDIF
  \END
\end{algo}\fi


Voyons le le cas d'une recherche fructueuse, par exemple si on cherche
la valeur~6 :
\begin{center}
  \includegraphics[scale=.46]{sda-recherche-dicho-0}
\end{center}
La valeur~6 est inférieure à la valeur du milieu (10), on réitère la
recherche dans la partie droite :
\begin{center}
  \includegraphics[scale=.46]{sda-recherche-dicho-1}  
\end{center}
La valeur~6 est supérieure à la valeur du milieu (4), on cherche donc
dans la partie droite, et on trouve. La recherche s'arrête :
\begin{center}
  \includegraphics[scale=.46]{sda-recherche-dicho-2}
\end{center}

Dans le cas d'une recherche infructueuse, les deux pointeurs de case
\texttt{début} et \texttt{fin} finiraient par se «~croiser~».

\iffalse
par exemple si on cherche la
valeur~19, on compare donc cette valeur avec la valeur milieu (10) :
\begin{center}
  \includegraphics[scale=.42]{sda-recherche-dicho-00}
\end{center}
Elle se trouve dans la partie droite. Dans cette partie, la valeur~19
est supérieure à la valeur milieu~(18) :
\begin{center}
  \includegraphics[scale=.44]{sda-recherche-dicho-10}  
\end{center}
On cherche donc à droite :
\begin{center}
  \includegraphics[scale=.44]{sda-recherche-dicho-20}
\end{center}
Dans cette partie, la valeur recherchée~19 est inférieure à la valeur
milieu~(29), on cherche donc dans la partie gauche qui est vide. 
\begin{center}
\includegraphics[scale=.44]{sda-recherche-dicho-30}
\end{center}
Le processus de recherche s'interrompt donc. 
\fi
\begin{propriete}
  On peut montrer, et on le comprend intuitivement, que la complexité
  de la recherche dichotomique est en $O(\log_2N)$ dans le pire des
  cas et en moyenne.
\end{propriete}
\pagebreak[4]
\begin{nota}
  Une complexité logarithmique ---~qui peut paraître au premier abord
  comme une formule mathématique obscure~--- implique que le nombre de
  comparaisons effectuées se comporte logarithmiquement (c'est-à-dire,
  comme «~l'inverse\footnote{Les profs de maths qui liront vont crier
    au scandale, certes ça n'est pas l'inverse mais la réciproque.}~»
  d'une puissance de~2.). Ainsi :
  \begin{itemize}
  \item pour $1024$ données, on fera $10$ comparaisons car $2^{10}=1024$
  \item pour $1048576$ données (en gros un millions), on fera $20$
    comparaisons, car $2^{20}=1048576$.
  \end{itemize}
  \continuenota Si bien qu'au lieu de comparer séquentiellement
  (c'est-à-dire, une donnée puis une autre) dans plusieurs milliards
  de données, on ne fera que quelques dizaines\footnote{Pour info
    $2^{30}=1073741824$} de comparaisons !
\end{nota}

\subsection{Recherche par table de hachage}
\index{recherche!table de hachage}
\index{hachage (recherche)}

\noindent\begin{minipage}{.65\textwidth}
  \setlength{\parindent}{20pt} On appelle \emph{table de hachage} une
  structure de données particulière permettant de réaliser des
  recherches de manière très efficace. Le principe général consiste à
  ranger la valeur recherchée~\valrech{} dans un tableau à la case
  dont le numéro est $h(\valrech)$, où $h$ est appelée \emph{fonction
    de hachage}. Nous verrons que la difficulté de la mise en place
  d'une table de hachage est précisément la définition de la
  fonction~$h$.

Nous présentons ici une étude de cas pour comprendre les mécanismes
entrant en jeu dans la table de hachage. Supposons par exemple que
l'on veuille effectuer des recherches sur des chaînes de caractères.
\end{minipage}%
\begin{minipage}{.35\textwidth}
  \begin{center}
    \includegraphics[width=.6\textwidth]{sda-hachage-exemple}
  \end{center}
\end{minipage}

\medskip
Comme l'illustre la figure ci-dessus, on a: 
\begin{itemize}
\item $h(\mbox{\ttfamily jean})=0$
\item $h(\mbox{\ttfamily jeanne})=2$
\item $h(\mbox{\ttfamily serge})=3$
\item $h(\mbox{\ttfamily maurice})=6$
\end{itemize}
Donc si on veut savoir si la chaîne «~\texttt{jean}~» est dans la
table, on calcule $h(\mbox{\ttfamily jean})$ qui donne~0 et en
examinant le contenu de la case~0, on trouvera la chaîne. C'est donc
une recherche fructueuse. À l'inverse, si on cherche la chaîne
«~\texttt{albert}~», la fonction $h(\mbox{\ttfamily albert})$ donnera
par exemple~8, et dans cette dernière case, aucune chaîne n'est
présente. «~albert~» n'est donc pas dans la table.

En supposant que la fonction $h$ associant une valeur à chaque données
soit bijective\footnote{C'est-à-dire qu'à une donnée correspond une
  seule valeur au travers de $h$ et inversement.} , le seul coût
entrant en jeu dans le recherche ou l'insertion est l'accès à une case
d'un tableau. Par conséquent :
\begin{propriete}
  Les opérations de recherche et d'insertion dans une table de hachage
  sont réalisées en temps constant (!). 
\end{propriete}
Nous allons voir par la suite, qu'il est impossible de trouver une
fonction de hachage bijective dans la plupart des cas.  Une «~bonne~»
fonction de hachage doit respecter certaines propriétés pour justifier
l'utilisation d'une table de hachage. Elle doit être :
\begin{itemize}
\item rapide à calculer ;
\item uniforme pour limiter les \emph{collisions}. 
\end{itemize}
Pour comprendre ce qu'est une collision, supposons que l'on dispose
d'une fonction de hachage $h(\mbox{\ttfamily chaîne})$ telle qu'après
avoir attribué une valeur à chaque lettre (a=1, b=2, etc.);
\begin{enumerate}
\item on fasse la somme des lettres de \texttt{chaîne} ;
\item on ajoute le nombre de lettres de \texttt{chaîne} ;
\item on calcule le reste de la division par $13$.
\end{enumerate}
Dans cas, le nombre d'entrées dans la table de hachage est 13, et :
\begin{itemize}
\item $h(\mbox{\ttfamily jean})=8$
\item $h(\mbox{\ttfamily serge})=7$
\item $h(\mbox{\ttfamily jane})=8$ $\longleftarrow$ \emph{collision}
\end{itemize}
Dans la mesure où les chaînes «~\texttt{jean}~» et «~\texttt{jane}~»
prennent la même valeur au travers de la fonction de hachage, on dit
qu'il se produit une \emph{collision} et il va falloir trouver une
astuce pour faire face à cette situation, puisqu'on ne peut stocker
deux valeurs dans la même case de la table de hachage.

\begin{figure}[bt]
  \begin{center}
    \includegraphics[width=.6\textwidth]{sda-hachage-collision}
  \end{center}
  \caption{Table de hachage avec collisions}
  \label{fig-sda-hachage-collisions}
\end{figure}

Une astuce possible pour gérer les collisions consiste à stocker
dans la case $n$ une séquence contenant toutes les chaînes~$c$ telles
que $h(c)=n$, c'est-à-dire prenant la même valeur~$n$ au travers de la
fonction~$h$. 
Comme l'illustre la figure~\vref{fig-sda-hachage-collisions}, on a :
\begin{itemize}
\item $h(\mbox{\ttfamily jean})=h(\mbox{\ttfamily jane})=h(\mbox{\ttfamily juan})=0$
\item $h(\mbox{\ttfamily jeanne})=h(\mbox{\ttfamily jennifer})=2$
\item $h(\mbox{\ttfamily serge})=3$
\item $h(\mbox{\ttfamily maurice})=h(\mbox{\ttfamily marcel})=6$
\end{itemize}
En d'autres termes, «~\texttt{jennifer}~» et «~\texttt{jeanne}~»
prennent la même valeur~2 au travers de la fonction~$h$, on construit
donc dans la case~2 de la table de hachage, une séquence contenant ces
deux chaînes. On fait la même chose à la case~6 pour «
\texttt{maurice}~» et «~\texttt{marcel}~», et pour «~\texttt{jane}~»
«~\texttt{juan}~» et «~\texttt{jean}~» à la case 0. 

Il faut bien évidemment modifier les algorithmes de recherche et
d'insertion pour tenir compte des collisions. Ainsi pour chercher une
valeur \valrech{}, il faudra si nécessaire chercher dans la séquence
associée à la case~$h(\valrech)$.  Pour ce qui est de la complexité :
\begin{propriete}
  Dans une table de hachage avec gestion de collision par séquence, la
  recherche et l'insertion ont une complexité de :
  \begin{itemize}
  \item $O(1)$ en moyenne ;
  \item $O(N)$ dans le pire des cas. 
  \end{itemize}
\end{propriete}

\iffalse
\begin{figure}[hbtp]
  \begin{center}
    \subfloat[non uniforme]{%
      \includegraphics[scale=.4]{sda-hachage-fonction-1}}
    \quad\quad
    \subfloat[uniforme]{%
      \includegraphics[scale=.4]{sda-hachage-fonction-2}}
  \end{center}
  \caption{Répartion des données dans une table de hachage}
  \label{fig-sda-hachage-fonction}
\end{figure}\fi
On comprend aisément que la complexité en moyenne sera
d'autant meilleure que les données seront uniformément réparties dans
la table.
% comme le montre la figure~\vref{fig-sda-hachage-fonction}. 
En outre le \emph{volume} de la table de hachage ne doit pas être trop
important pour que la table puisse effectivement être exploitée en
mémoire. 



\subsection{Recherche par arbre binaire}
\label{sec-recherche-abr}
\index{recherche!arbre binaire}
\index{arbre binaire!de recherche}

Nous présentons dans ce paragraphe une méthode de recherche s'appuyant
sur une structure de données particulière : \emph{l'arbre binaire de
  recherche}. Un arbre binaire de recherche (ABR) est une structure de
données abstraite dérivée de l'arbre binaire telle que :
\begin{center}
  \includegraphics[scale=.38]{sda-abr-filiation}
\end{center}
C'est-à-dire, telle qu'en chaque sommet $N$, le sous-arbre gauche
contient des valeurs inférieures à celle contenue dans $N$, et le
sous-arbre droit des valeurs supérieures. 
\begin{nota}
  Dans l'arbre \emph{complet} de la figure~\vref{subfig-abr-complet},
  chaque fils possède ses deux n\oe uds. En notant $N$ le nombre total
  de n\oe uds et $h$ la hauteur de l'arbre, on a :
  \begin{itemize}
  \item $N = 2^h - 1$
  \item $h = \log_2{N+1}$
  \end{itemize}
  \continuenota%
  où $\log_2$ est le logarithme en base~2.
\end{nota}
Les opérations associées à cette structure sont :
\begin{itemize}
\item Ajout et suppression d'une valeur ;
\item Recherche d'une valeur. 
\end{itemize}

La figure~\vref{fig-sda-abr-exemples} montre deux arbres binaires de
recherche contenant tous deux les mêmes données (\texttt{A},
\texttt{B}, ... , \texttt{G}). L'ordre utilisé est l'ordre
lexicographique (\texttt{A} est inférieur à \texttt{B}).
\begin{figure}[tb]
  \begin{center}
    \subfloat[]{\includegraphics[scale=.35]{sda-abr-0}
      \label{subfig-abr-complet}}\quad
    \subfloat[]{\includegraphics[scale=.35]{sda-abr-1}}\\
%    \subfloat[]{\includegraphics[scale=.35]{sda-abr-2}}\quad
%    \subfloat[]{\includegraphics[scale=.35]{sda-abr-3}\label{subfig-abr-degen}}
  \end{center}
  \caption{Exemples d'arbres binaires de recherche : dans ces deux
    arbres, pour chaque nœud, le sous-arbre de gauche contient des
    valeurs inférieures, celui de droite des valeurs supérieures.}
  \label{fig-sda-abr-exemples}
\end{figure}


Pour savoir si une valeur \valrech{} est présente dans l'arbre binaire
de recherche, on va effectuer un parcours de l'arbre en partant de la
racine. Si la valeur contenue dans la racine n'est pas~\valrech{} on
continue la recherche \emph{de la même manière}\footnote{Les
  algorithmiciens disent \emph{récursivement}.} à gauche ou à droite
selon que \valrech{} est inférieure ou supérieure à la valeur contenue
dans la racine.

\noindent\begin{minipage}{.6\textwidth}
Par exemple pour une recherche (infructueuse) de la valeur~\texttt{F},
on commence donc par comparer \texttt{F} à la valeur de la
racine~\texttt{M}
\end{minipage}
\begin{minipage}{.4\textwidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-abr-recherche-1}
  \end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  La valeur~\texttt{F} est inférieure à~\texttt{M}, on réitère donc la
  recherche à partir du fils gauche contenant la valeur~\texttt{E}
\end{minipage}
\begin{minipage}{.4\textwidth}
\begin{center}
  \includegraphics[scale=.4]{sda-abr-recherche-2}
\end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  On «~arrive~» donc sur le sommet contenant la valeur~\texttt{G} et on
  compare cette valeur à~\texttt{F}
\end{minipage}
\begin{minipage}{.4\textwidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-abr-recherche-3}
  \end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  La valeur~\texttt{F} est inférieure à~\texttt{G} on cherche donc à
  partir du fils gauche du sommet contenant~\texttt{G}
\end{minipage}
\begin{minipage}{.4\textwidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-abr-recherche-4}
  \end{center}
\end{minipage}

Ce sommet n'existe pas, la recherche s'arrête donc ici, sans avoir
trouvé la valeur~\texttt{F}. 

\iffalse
Dans le cas d'un recherche fructueuse on cherche la valeur~\texttt{T},
on commence comme précédemment à la racine contenant la
valeur~\texttt{M} et on compare les deux valeurs :
\begin{center}
  \includegraphics[scale=.4]{sda-abr-recherche-1b}
  \includegraphics[scale=.4]{sda-abr-recherche-2b}
  \includegraphics[scale=.4]{sda-abr-recherche-3b}
\end{center}
Lors du parcours on rencontrera les sommets contenant les
valeurs~\texttt{R} puis~\texttt{T}, la valeur recherchée. 
\fi

\subsubsection{Insertion dans un ABR}
\label{sec-recherche-insertion-abr}
\index{arbre binaire!insertion}
Le principe de l'insertion est analogue à celui de la recherche d'une
valeur ne se trouvant pas dans l'arbre. Nous avons vu précédemment que
lorsqu'une recherche était infructueuse, le parcours s'arrêtait sur
une feuille de l'arbre (c'est-à-dire un sommet terminal). L'opération
d'insertion devra alors insérer un nouveau sommet en guise de nouveau
fils de ce sommet terminal. 

\noindent\begin{minipage}{.6\textwidth}
  Pour comprendre le principe de l'insertion dans un arbre binaire de
  recherche, nous étudierons un exemple. On veut insérer la
  valeur~\texttt{S} dans l'arbre binaire utilisé dans les exemples
  précédents. On commence par comparer cette valeur à la racine
\end{minipage}
\begin{minipage}{.4\textwidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-abr-insertion-1}
  \end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  Étant donné que la valeur~\texttt{S} est supérieure à la valeur se
  trouvant dans la racine, on continue à droite au sommet contenant la
  valeur~\texttt{R}
\end{minipage}
\begin{minipage}{.4\textwidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-abr-insertion-2}
  \end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  De la même manière, on «~tourne à droite~» vu que \texttt{S} est
  supérieur à~\texttt{R}
\end{minipage}
\begin{minipage}{.4\textwidth}
\begin{center}
  \includegraphics[scale=.4]{sda-abr-insertion-3}
\end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  Arrivé au sommet contenant la valeur~\texttt{T}, on va réitérer le
  processus avec le fils gauche (puisque \texttt{S} est inférieur à
  \texttt{T}). Or ce fils n'existe pas, on choisit donc cet emplacement
  pour greffer un nouveau sommet contenant la valeur~\texttt{S}.
\end{minipage}
\begin{minipage}{.4\textwidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-abr-insertion-4}
  \end{center}
\end{minipage}

\subsubsection{Complexité}
\index{arbre binaire!complexité}

Pour estimer la complexité des opérations d'insertion et de recherche
sur un ABR, il faut dans un premier temps observer que pour ces deux
opérations on effectue un parcours lors duquel on rencontre à peu près
autant d'éléments qu'il y a de niveaux dans l'arbre. Nous avons vu au
début du \S~\vref{sec-recherche-abr}, que le nombre de niveaux d'un arbre binaire
complet composé de $n$ sommets est $\log_2(n+1)$. 
% On peut ensuite se demander quel est le nombre de sommet pour un
% arbre binaire \emph{complet} comme celui de la
% figure~\vref{subfig-abr-complet} :
% \begin{itemize}
% \item il y en a $1$ au niveau~0 (la racine) ;
% \item il y en a $2$ au niveau~1 ;
% \item il y en a $4$ au niveau~2 ;
% \item on peut montrer qu'il en a $2^h$ au niveau $h$ (par exemple par
%   récurence). 
% \end{itemize}
% On peut également montrer qu'au total il y a $2^H-1$ si $H$ est le
% nombre de niveaux de l'arbre binaire complet. On peut enfin en déduire
% que pour un arbre binaire complet le nombre de niveaux est égal
% à~$\log_2(N+1)$ où $N$ est le nombre de sommets. 
Par conséquent :
\begin{propriete}
  Pour un arbre binaire de recherche contenant $N$ données (c.-à-d. $N$
  sommets), les opérations d'insertion et de recherche sont en :
  \begin{itemize}
  \item $O(\log_2N)$ en moyenne ;
  \item $O(N)$ dans le pire des cas (arbre filiforme). 
  \end{itemize}
\end{propriete}

\subsubsection{Pourquoi la complexité $\log_2$ est-elle «~tip-top~» ?}
\label{sec-sda-log2}

À l'instar de la recherche
\voir{sec-sda-recherche-sequence}{dichotomique}, la recherche dans un
arbre binaire, s'il est complet, sera de l'ordre de $\log_2(N)$ dans
le pire des cas, le cas d'une recherche infructueuse. $N$ est ici le
nombre de données de l'arbre, c'est-à-dire le volume de données parmi
lesquelles on cherche. Le tableau ci-dessous montre la correspondance
entre hauteur $H$ et nombre de n\oe uds $N$ :
\begin{center}
  \begin{tabular}{c|r|c}
    Arbre & $N$ & hauteur $H$ \\
    \hline
    \includegraphics[scale=.2]{sda-abr-h2} & 3   & $2 = \log_2(3+1)$ \\
    \includegraphics[scale=.2]{sda-abr-h3} & 7   & $3 = \log_2(7+1)$ \\
    \includegraphics[scale=.2]{sda-abr-h4} & 15  & $4=  \log_2(15+1)$\\
    ...& ... & ... \\
    ...& 1023 & $10=\log_2(1023+1)$\\
    ...& 1048575 & $20=\log_2(1048575+1)$\\
    ...& 1073741823 & $30=\log_2(1073741823+1)$\\
    \hline
  \end{tabular}
\end{center}
\enlargethispage{1cm}
\begin{nota}
  Comme pour la recherche dichotomique, il faut donc se persuader que
  si vous aviez \emph{un milliard de données} et vous êtiez capable de
  les ranger dans un arbre binaire complet (ce qui n'est pas une
  opération simple), vous n'auriez qu'au pire 30 comparaisons à faire
  pour répondre à la question : «~cette valeur est-elle parmi mon
  milliard d'informations~» !
\end{nota}
\clearpage

\subsection{Autres arbres}

Si vous ne vous êtes pas endormi jusqu'ici, vous aurez compris que le
défaut majeur d'un arbre binaire de recherche est qu'il peut être
réduit à une simple séquence si les données se présentent «~mal~»
pendant la construction. À titre d'exemple, les données E, G, L, P,
présentées dans cet ordre, donneraient en utilisant l'algorithme
d'insertion dans un ABR (\S~\vref{sec-recherche-insertion-abr}) :
\begin{center}
  \begin{tabular}{ccccccc}
  \includegraphics[scale=.3]{sda-abr-pb-1}&
  puis : &
  \includegraphics[scale=.3]{sda-abr-pb-2}&
  puis : &
  \includegraphics[scale=.3]{sda-abr-pb-3}&
  puis :&
  \includegraphics[scale=.3]{sda-abr-pb-4}
\end{tabular}
\end{center}
On comprend donc que l'arbre ainsi créé n'a plus aucun intérêt puisque
la recherche d'un élément demande alors de parcourir tous ses n\oe
uds. La complexité devient alors $O(N)$, en d'autres termes celle de
la recherche séquentielle dans une séquence triée.

\subsubsection{Arbres AVL}
\index{arbre binaire!AVL}

C'est la raison pour laquelle Georgii \textsc{Adelson-Velsky} et
Evguenii \textsc{Landis} ont conçu au début des années~60, des arbres
portant leurs noms : les arbres \emph{AVL}. Ces arbres ont la
particularité de se «~ré-équilibrer~» automatiquement pendant les
opérations d'insertions (et de suppression). 

Bien évidemment ces équilibrages ne doivent pas être couteux en
calcul. Dans le cas contraire cela n'aurait plus d'intérêt d'utiliser
la structure. Les deux compères cités plus haut ont réussi le tour de
force de réaliser les opérations de ré-équilibrage en $O(log_2(N))$.

\noindent\begin{minipage}{.6\textwidth}
  À titre d'exemple, nous montrons ici un arbre AVL dans lequel on
  vient d'insérer la valeur~\texttt{F}. On constate que le sous-arbre gauche
  de la racine est plus haut que le sous-arbre droit. On va donc
  procéder à un «~rotation gauche~» autour du n\oe ud~\texttt{G}.
\end{minipage}%
\begin{minipage}{.4\textwidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-avl-3}
  \end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  Le résultat de cette rotation est indiqué ci-contre. On réalise
  alors une «~rotation droite~» autour du n\oe ud~\texttt{L}.
\end{minipage}%
\begin{minipage}{.4\textwidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-avl-5}
  \end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  Rotation qui donne l'arbre ci-contre.
\end{minipage}%
\begin{minipage}{.4\textwidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-avl-6}
  \end{center}
\end{minipage}

\subsubsection{B-Arbres}
\label{sec-chercher-b-arbres}
\index{arbres B}
\index{B-arbres}

D'autres arbres sont nés de l'imagination des chercheurs en
algorithmie et parmi ceux-là les \emph{B-arbres} qui sont souvent
utilisés pour l'indexation des données dans un système de gestion de
base de données. On ne rentrera pas dans le détail de ces arbres, dont le
principe est dû à Rudolf \textsc{Bayer} au début des années~70, mais
donnerons quelques grands principes :
\begin{itemize}
\item les B-arbres ne sont pas des arbres binaires mais des arbres 
  dont les n\oe uds peuvent avoir plusieurs fils ;
\item les n\oe uds eux-mêmes peuvent contenir plusieurs valeurs ;
\item les B-arbres sont \emph{constamment équilibrés}, c'est ce qui
  rend la recherche dans ceux-ci particulièrement efficaces.
\end{itemize}

\noindent\begin{minipage}{.6\textwidth}
  À titre d'exemple, dans le B-arbre ci-contre, le n\oe ud racine
  contient deux valeurs (\texttt{D} et~\texttt{N}) et ses fils sont
  des n\oe uds contenant respectivement des valeurs :
  \begin{itemize}
  \item inférieures à \texttt{D}
  \item comprises entre \texttt{D} et \texttt{N}
  \item supérieures à \texttt{N}
  \end{itemize}
\end{minipage}%
\begin{minipage}{.4\textwidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-btree-0}
  \end{center}
\end{minipage}
\par\noindent On peut imaginer qu'il s'agit d'une sorte de généralisation de l'arbre
binaire de recherche et que pour effectuer une recherche dans une
telle structure, on procédera comme suit :
\begin{itemize}
\item si la valeur recherchée est dans la racine on renvoie «~c'est bon, j'ai trouvé !~» ;
\item sinon on la cherche dans le fils adapté, c'est-à-dire celui
  contenant des valeurs dans un intervalle encadrant la valeur
  recherchée ;
\item et ainsi de suite, récursivement, avec les n\oe ud de niveaux
  inférieurs.
\end{itemize}
Par exemple si on cherche à savoir si la valeur~\texttt{G} est dans
l'arbre, on traversera successivement la racine puis le n\oe ud
contenant \texttt{F,J}. Le processus s'arrêtera puisque ce dernier n\oe
ud n'a pas de fils comportant des valeurs comprises entre~\texttt{F}
et~\texttt{J}, où aurait pu se trouver la valeur
recherchée~\texttt{G}.

\medskip
\noindent\begin{minipage}{.5\linewidth}
  Pour insérer dans un B-arbre, par exemple la valeur~\texttt{E}, on
  commencera par l'insérer comme expliqué dans le précédent
  paragraphe.
\end{minipage}%
\begin{minipage}{.5\linewidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-btree-2}
  \end{center}
\end{minipage}

\medskip
\noindent\begin{minipage}{.5\linewidth}
  Puis, pour conserver le degré du B-arbre, c'est-à-dire, s'assurer que
  chaque n\oe{}ud contient un maximum de 2 valeurs, on fera remonter la
  valeur centrale vers le niveau supérieur.
\end{minipage}%
\begin{minipage}{.5\linewidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-btree-3}
  \end{center}
\end{minipage}

\medskip
\noindent\begin{minipage}{.5\linewidth}
  Il est ensuite probable d'avoir à répéter cette opération de «
  remontée~» pour le niveau supérieur.
\end{minipage}%
\begin{minipage}{.5\linewidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-btree-4}
  \end{center}
\end{minipage}

\medskip
\noindent\begin{minipage}{.5\linewidth}
  On obtiendra finalement le B-arbre ci-contre, après avoir fait
  remonter la valeur centrale du n\oe ud arrivé ici à saturation.
\end{minipage}%
\begin{minipage}{.5\linewidth}
  \begin{center}
    \includegraphics[scale=.4]{sda-btree-5}
  \end{center}
\end{minipage}


\endinput
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../framabook"
%%% End: 
